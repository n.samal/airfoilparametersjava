package src.test.java.geometry;

import src.main.java.geometry.Point;

import java.util.Optional;

// Junit specifics
// https://junit.org/junit5/docs/5.0.1/api/org/junit/jupiter/api/Assertions.html
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Before;
import org.junit.Test;

public class PointTest {

    // Use realistic value bounds
    final double MAX_VALUE = 1e9;
    final double MIN_VALUE = -1e9;
    
    private double tol = 1e-6; // assertion tolerance criteria
    private Point point;
    private Point other;

    @Before
    public void runBeforeTest(){
        /* Initialize values randomly prior to test
        @see: 
            https://docs.oracle.com/javase/7/docs/api/java/lang/Math.html
            https://docs.oracle.com/javase/7/docs/api/java/lang/Double.html
        */

        // Scale the random number between set bounds
        // we can also use Double Bounds ie: Double.MIN_VALUE
        double scale = ((this.MAX_VALUE - this.MIN_VALUE) + 1) + this.MIN_VALUE;
        this.point = new Point(Math.random() * scale, Math.random() * scale, Math.random() * scale);
        this.other = new Point(Math.random() * scale, Math.random() * scale, Math.random() * scale);
    }

    // Copy
    @Test 
    public void testDeepCopy(){

        // Create a copy of 
        Point point2 = this.point.deepCopy();

        // Modify original point
        this.point.setI(Math.random());
        this.point.setJ(Math.random());
        this.point.setK(Math.random());

        // Verify original is not changed
        assertNotEquals(this.point.getI(), point2.getI(), this.tol);
        assertNotEquals(this.point.getJ(), point2.getJ(), this.tol);
        assertNotEquals(this.point.getK(), point2.getK(), this.tol);
    }

    // Coordinates

    @Test
    public void testSetI(){

        // Modify point value
        double value = Math.random();
        this.point.setI(value);

        // Check value against tolerance
        assertEquals(value, this.point.getI(), this.tol);
    }

    @Test
    public void testSetJ(){

        // Modify point value
        double value = Math.random();
        this.point.setJ(value);

        // Check value against tolerance
        assertEquals(value, this.point.getJ(), this.tol);
    }
    
    @Test
    public void testSetK(){

        // Modify point value
        double value = Math.random();
        this.point.setK(value);

        // Check value against tolerance
        assertEquals(value, this.point.getK(), this.tol);
    }

    @Test
    public void testSetCoords(){

        // Modify point value
        double [] coords = new double [] {Math.random(), Math.random(), Math.random()};
        this.point.setCoords(coords);

        // Check value against tolerance
        assertArrayEquals(coords, this.point.getCoords(), this.tol);
    }

    // Arithmetic

    @Test
    public void testMathAdd(){

        // Compute new point
        Point product = this.point.add(this.other);

        // Verify math
        assertEquals(this.point.getI() + this.other.getI(), product.getI(), this.tol);
        assertEquals(this.point.getJ() + this.other.getJ(), product.getJ(), this.tol);
        assertEquals(this.point.getK() + this.other.getK(), product.getK(), this.tol);
    }

    @Test
    public void testMathSubtract(){

        // Compute new point
        Point product = this.point.subtract(this.other);

        // Verify math
        assertEquals(this.point.getI() - this.other.getI(), product.getI(), this.tol);
        assertEquals(this.point.getJ() - this.other.getJ(), product.getJ(), this.tol);
        assertEquals(this.point.getK() - this.other.getK(), product.getK(), this.tol);
    }

    @Test
    public void testMathMultiply(){

        // Compute new point
        Point product = this.point.multiply(this.other);

        // Verify math
        assertEquals(this.point.getI() * this.other.getI(), product.getI(), this.tol);
        assertEquals(this.point.getJ() * this.other.getJ(), product.getJ(), this.tol);
        assertEquals(this.point.getK() * this.other.getK(), product.getK(), this.tol);
    }
    
    @Test
    public void testMathDivide(){

        // Compute new point
        Point product = this.point.divide(this.other);

        // Verify math
        assertEquals(this.point.getI() / this.other.getI(), product.getI(), this.tol);
        assertEquals(this.point.getJ() / this.other.getJ(), product.getJ(), this.tol);
        assertEquals(this.point.getK() / this.other.getK(), product.getK(), this.tol);
    }

    // Functions
    @Test
    public void testMagnitude(){

        // Verify math
        double expected = Math.sqrt( Math.pow(this.point.getI(), 2) + Math.pow(this.point.getJ(), 2) + Math.pow(this.point.getK(), 2));
        assertEquals(expected, this.point.getMag(), this.tol);
    }

    @Test
    public void testDistance1dI(){

        double expected = Math.sqrt(Math.pow(this.point.getI() - this.other.getI(), 2));
        assertEquals(expected, this.point.distI(this.other), this.tol);
    }

    @Test
    public void testDistance1dJ(){

        double expected = Math.sqrt(Math.pow(this.point.getJ() - this.other.getJ(), 2));
        assertEquals(expected, this.point.distJ(this.other), this.tol);
    }

    @Test
    public void testDistance1dK(){

        double expected = Math.sqrt(Math.pow(this.point.getK() - this.other.getK(), 2));
        assertEquals(expected, this.point.distK(this.other), this.tol);
    }

    @Test
    public void testDistance2dIJ(){

        double expected = Math.sqrt(Math.pow(this.point.getI() - this.other.getI(), 2) + Math.pow(this.point.getJ() - this.other.getJ(), 2));
        assertEquals(expected, this.point.distIJ(this.other), this.tol);
    }

    @Test
    public void testDistance2dIK(){

        double expected = Math.sqrt(Math.pow(this.point.getI() - this.other.getI(), 2) + Math.pow(this.point.getK() - this.other.getK(), 2));
        assertEquals(expected, this.point.distIK(this.other), this.tol);
    }

    @Test
    public void testDistance2dJK(){

        double expected = Math.sqrt(Math.pow(this.point.getJ() - this.other.getJ(), 2) + Math.pow(this.point.getK() - this.other.getK(), 2));
        assertEquals(expected, this.point.distJK(this.other), this.tol);
    }

    @Test
    public void testDistance3D(){

        double expected = Math.sqrt(Math.pow(this.point.getI() - this.other.getI(), 2) + Math.pow(this.point.getJ() - this.other.getJ(), 2) + Math.pow(this.point.getK() - this.other.getK(), 2));
        assertEquals(expected, this.point.dist(this.other), this.tol);
    }

    // Tranformations: Reflections

    @Test
    public void testReflectIOrigin(){

        // Reflect about origin
        Optional <Point> center = Optional.empty();
        Point point = this.point.reflectI(center);

        // Check value
        double [] coordsCheck = new double [] {-this.point.getI(), this.point.getJ(), this.point.getK()};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testReflectICenter(){

        // Reflect about center
        Optional <Point> center = Optional.of(this.other);
        Point point = this.point.reflectI(center);

        // Check value
        double [] coordsCheck = new double [] {-(this.point.getI() - this.other.getI()) + this.other.getI(), this.point.getJ(), this.point.getK()};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testReflectJOrigin(){

        // Reflect about origin
        Optional <Point> center = Optional.empty();
        Point point = this.point.reflectJ(center);

        // Check value
        double [] coordsCheck = new double [] {this.point.getI(), -this.point.getJ(), this.point.getK()};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testReflectJCenter(){

        // Reflect about center
        Optional <Point> center = Optional.of(this.other);
        Point point = this.point.reflectJ(center);

        // Check value
        double [] coordsCheck = new double [] {this.point.getI(), -(this.point.getJ() - this.other.getJ()) + this.other.getJ(), this.point.getK()};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testReflectKOrigin(){

        // Reflect about origin
        Optional <Point> center = Optional.empty();
        Point point = this.point.reflectK(center);

        // Check value
        double [] coordsCheck = new double [] {this.point.getI(), this.point.getJ(), -this.point.getK()};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testReflectKCenter(){

        // Reflect About a center point
        Optional <Point> center = Optional.of(this.other);
        Point point = this.point.reflectK(center);

        // Check value
        double [] coordsCheck = new double [] {this.point.getI(), this.point.getJ(), -(this.point.getK() - this.other.getK()) + this.other.getK()};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    // Transformations: Scale
    
    @Test
    public void testScaleDoubleOrigin(){

        // Reflect About a center point
        Optional <Point> center = Optional.empty();
        double scale = Math.random();
        Point point = this.point.scale(scale, center);

        // Check value
        double [] coordsCheck = new double [] {this.point.getI() * scale, this.point.getJ() * scale, this.point.getK() * scale};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testScaleDoubleCenter(){

        // Reflect about a center point
        Optional <Point> center = Optional.of(this.other);
        double scale = Math.random();
        Point point = this.point.scale(scale, center);

        // Check value
        double [] coordsCheck = new double [] {
            (this.point.getI() - this.other.getI()) * scale + this.other.getI(),
            (this.point.getJ() - this.other.getJ()) * scale + this.other.getJ(),
            (this.point.getK() - this.other.getK()) * scale + this.other.getK(),
            };
        
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testScaleArrayOrigin(){

        // Scale about origin
        Optional <Point> center = Optional.empty();
        double [] scales = new double [] {Math.random(), Math.random(), Math.random()};
        Point point = this.point.scale(scales, center);

        // Check value
        double [] coordsCheck = new double [] {this.point.getI() * scales[0], this.point.getJ() * scales[1], this.point.getK() * scales[2]};
        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
    }

    @Test
    public void testScaleArrayCenter(){

        // Scale about center
        Optional <Point> center = Optional.of(this.other);
        double [] scales = new double [] {Math.random(), Math.random(), Math.random()};
        Point point = this.point.scale(scales, center);

        // Check value
        double [] coordsCheck = new double [] {
            (this.point.getI() - this.other.getI()) * scales[0] + this.other.getI(),
            (this.point.getJ() - this.other.getJ()) * scales[1] + this.other.getJ(),
            (this.point.getK() - this.other.getK()) * scales[2] + this.other.getK(),
        };

        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
     
    }

    // Transformations: Translation
    @Test
    public void testTranslation(){

        // Scale about center
        double [] shifts = new double [] {Math.random(), Math.random(), Math.random()};
        Point point = this.point.translate(shifts[0], shifts[1], shifts[2]);

        // Check value
        double [] coordsCheck = new double [] {
            this.point.getI() + shifts[0],
            this.point.getJ() + shifts[1],
            this.point.getK() + shifts[2],
        };

        assertArrayEquals(point.getCoords(), coordsCheck, this.tol);
     
    }

    // Transformations: Rotations

    // TODO: RotateI, RotateJ, RotateK about center
    // TODO: RotateI, RotateJ, RotateK about origin

}