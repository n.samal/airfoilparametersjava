package src.main.java.geometry;

import org.jblas.DoubleMatrix;
import java.util.Optional;

import src.main.java.geometry.Point;

class Curve{

    private Point [] points;
    private double [] i;
    private double [] j;
    private double [] k;
    private double [] s;

    // Constructors
    public Curve(Point [] points){

        this.setPoints(points);
    }

    // Point setters & getters
    public void setPoint(int ix, Point point){
        
        // Set primitives directly
        this.i[ix] = point.getI();
        this.j[ix] = point.getJ();
        this.k[ix] = point.getK();
        this.s[ix] = Double.NaN;

        // Copy objects
        this.points[ix] = point.deepCopy();
    }

    public void setPoints(Point [] points){

        // Set all points
        for (int ix = 0; ix < points.length; ix++){
            this.setPoint(ix, points[ix]);
        }
    }

    public void setPoints(int startIndex, int stopIndex, Point [] points){

        // Set a slice of points
        for (int ix = startIndex; ix < stopIndex; ix++){
            this.setPoint(ix, points[ix]);
        }
    }

    public Point getPoint(int ix){
        // Grab a copy of the original
        return this.points[ix].deepCopy();
    }

    public Point [] getPoints(int startIndex, int stopIndex){

        // Create empty array
        Point [] array = new Point [stopIndex - startIndex];

        // Set a slice of points
        for (int ix = startIndex; ix < stopIndex; ix++){
            array[ix] = this.getPoint(ix);
        }

        return array;
    }

    public Point [] getPoints(){

        // Grab all points
        return this.getPoints(0, this.points.length);
    }

    // Properties: short hand
    public Point getStart(){
        // Grab the first point in our curve
        return getPoint(0);
    }

    public Point getStop(){
        // Grab the last point in our curve
        return getPoint(this.points.length - 1);
    }

    public void setStart(Point point){
        
        // Set the first point
        this.setPoint(0, point);
    }

    public void setStop(Point point){
        
        // Set the last point
        this.setPoint(this.points.length - 1, point);
    }

    // Functions
    public double [] angle(){
        /* Compute the angle between vectors

        The angle between vectors can be calculated using the dot product.
        We know the dot product is a projection of one vector onto another.
        We also know that the cosine has a very similar relationship
        ie: A basic triangle. A*cos(theta) = B

        @param show: plot the calculated distribution
        @return double []: angle between line segments (radians)
        @see:
            https://www.wikihow.com/Find-the-Angle-Between-Two-Vectors
            https://math.stackexchange.com/questions/2610186/discrete-points-curvature-analysis#2620690
            https://en.wikipedia.org/wiki/Direction_cosine
        TODO: add plotting capability
        */
        double [] theta = new double [this.points.length];

        theta[0] = Double.NaN;

        for (int ix = 1; ix < this.points.length; ix++){

            // Vectors adjacent to point
            Point zkm1 = this.points[ix - 1].subtract(this.points[ix]);
            Point zkp1 = this.points[ix + 1].subtract(this.points[ix]);

            // Convert to double matrix for easy dot product
            DoubleMatrix zkm1M = new DoubleMatrix(zkm1.getCoords());
            DoubleMatrix zkp1M = new DoubleMatrix(zkp1.getCoords());

            // Angle projection
            double num = zkm1M.dot(zkp1M);
            double den = zkm1.getMag() * zkp1.getMag();

            // Compute angle between vectors
            theta[ix] = Math.acos(num / den);
        }

        return theta;
    }

    public void reverse(){

        // Reverse the order of points
        for(int ix = 0; ix < this.points.length / 2; ix++){

            // Create temporary object
            Point temp = this.points[ix];
           
            // Swap locations i and j
            this.points[ix] = this.points[this.points.length - ix - 1];
            this.points[this.points.length - ix - 1] = temp;
           }
    }

    // Normalized distances & interpolation
    public double [] getNormalizedSDistance(){
        /* Calculate the normalized cummulative distance in 3D aka s distance*/

        // Initialize empty array
        double [] s = new double [this.points.length];

        for (int ix = 1; ix < this.points.length; ix++){

            // Compute distance between points
            double dist = this.points[ix].dist(this.points[ix - 1]);
            
            // Store cummulative distance
            s[ix] = s[ix - 1] + dist;
        }

        // Grab normalizing value
        double s_max = s[this.points.length] * 1.0;

        // Normalize distances between 0.0 and 1.0
        for (int ix = 0; ix < this.points.length; ix++){
            s[ix] = s[ix] / s_max;
        }

        return s;
    }

    // TODO: spline interpolation (http://commons.apache.org/proper/commons-math/javadocs/api-3.3/org/apache/commons/math3/analysis/interpolation/SplineInterpolator.html)

    // Transformations
    // TODO: https://www.concretepage.com/java/jdk-8/function-apply-in-java-8
    // TODO: variable number of arguments https://stackoverflow.com/questions/2330942/java-variable-number-or-arguments-for-a-method
    private Curve general_transform(String function){
        /*Apply transformation function to all points in the curve

        @param function: name of function to apply to all points
        @returns: transformed curve
        @throws:
            If invalid transformation is applied
        */

        // Initialize empty array
        Point [] points_new = new Point [this.points.length];

        // Iterate through all points and apply transformation
        for (int ix = 0; ix < this.points.length; ix++){

            // Apply function to point and save
            points_new[ix] = this.points[ix];

        }

        return new Curve(points_new);
    }


    // TODO: apply generalized transform method

    public Curve translate(double i, double j, double k){
        /*Translate all points in the curve

        @param i: offset in i direction
        @param j: offset in j direction
        @param k: offset in k direction
        @returns: translated curve
        */

        // Initialize empty array
        Point [] points_new = new Point [this.points.length];

        // Iterate through all points and apply transformation
        for (int ix = 0; ix < this.points.length; ix++){

            // Apply function to point and save
            points_new[ix] = this.points[ix].translate(i, j, k);

        }

        return new Curve(points_new);

    }

    public Curve scale(double i, double j, double k, Optional <Point> centerPoint){
        /*Translate all points in the curve

        @param i: offset in i direction
        @param j: offset in j direction
        @param k: offset in k direction
        @returns: translated curve
        */

        // Initialize empty array
        Point [] points_new = new Point [this.points.length];

        // Iterate through all points and apply transformation
        for (int ix = 0; ix < this.points.length; ix++){

            // Apply function to point and save
            points_new[ix] = this.points[ix].scale(new double [] {i, j, k});

        }

        return new Curve(points_new);

    }
}