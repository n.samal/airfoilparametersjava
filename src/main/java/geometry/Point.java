package src.main.java.geometry;

import java.util.Arrays;
import java.util.Optional;
import org.jblas.DoubleMatrix;

public class Point {

    // Instance variables
    private double i;
    private double j;
    private double k;
    private double [] coords;
    private DoubleMatrix coords_hom;

    // Class Constructors
    public Point(double i, double j, double k){
        
        // Define instance vars
        this.i = i;
        this.j = j;
        this.k = k;
        this.coords = new double [] {i, j, k};
        this.coords_hom = new DoubleMatrix(new double [][] {{i}, {j}, {k}, {1.0}});
    }

    public Point(double [] coords){
        // Create point using input coordinates via original constructor
        this(coords[0], coords[1], coords[2]);
    }

    @Override
    public String toString(){

        // Generate coordinate formatting ie: [1.00, 2.00, 3.00]
        String s = String.format("[%1$g, %2$g, %3$g]", this.i, this.j, this.k);
        
        // Output with class name ie: Point[1.00, 2.00, 3.00]
        return getClass().getName() + s;
    }

    public Point deepCopy(){
        // Return a deep copy of the object (ie: references broken)
        return new Point(this.getI(), this.getJ(), this.getK());
    }

    // Coordinate value setters
    public void setI(double i){
        this.coords[0] = i;
        this.coords_hom.put(0, 0, i);
        this.i = i;
    }
    public void setJ(double j){
        this.coords[1] = j;
        this.coords_hom.put(1, 0, j);
        this.j = j;
    }
    public void setK(double k){
        this.coords[2] = k;
        this.coords_hom.put(2, 0, k);
        this.k = k;
    }

    // Coordinate value getters
    public double getI(){
        return this.i;
    }
    public double getJ(){
        return this.j;
    }
    public double getK(){
        return this.k;
    }

    public double getCoordByIndex(int ix){

        Double value = null;

        // Grab coordinate value by index
        switch (ix){
            case 0: 
                value = this.getI();
                break;
            case 1: 
                value = this.getJ();
                break;
            case 2: 
                value = this.getK();
                break;
            default:
                throw new IllegalArgumentException("Index must be 0, 1 or 2 instead of " + ix);
        }

        return value;
    }

    // Coordinate getters
    public double [] getCoords(){
        
        // Use a copy to avoid internal referencing
        double [] copy = Arrays.copyOf(this.coords, this.coords.length);
        
        return copy;
    }

    // Coordinate setters
    public void setCoords(double [] coords){
        // TODO: restrict input to 3 values 

        // Set values
        this.setI(coords[0]);
        this.setJ(coords[1]);
        this.setK(coords[2]);
    } 

    // Basic arithmetic
    public Point add(Point p){
        
        // Add point coordinates together
        Point pnew = new Point(this.i + p.i, this.j + p.j, this.k + p.k);
        
        return pnew;
    }

    public Point subtract(Point p){
        
        // Subtract point coordinates
        Point pnew = new Point(this.i - p.i, this.j - p.j, this.k - p.k);
        
        return pnew;
    }
    
    public Point multiply(Point p){
        
        // Multiply point coordinates together
        Point pnew = new Point(this.i * p.i, this.j * p.j, this.k * p.k);
        
        return pnew;
    }
    
    public Point divide(Point p){
        
        // Divide point coordinates
        Point pnew = new Point(this.i / p.i, this.j / p.j, this.k / p.k);
        
        return pnew;
    }

    // Basic arithmetic: static functions (remove if unnecessary)
    public static Point add(Point p1, Point p2){

        // Add point coordinates together
        Point p = new Point(p1.i + p2.i, p1.j + p2.j, p1.k + p2.k);
        
        return p;
        
    }
    
    public static Point subtract(Point p1, Point p2){

        // Subtract point coordinates
        Point p = new Point(p1.i - p2.i, p1.j - p2.j, p1.k - p2.k);
        
        return p;
        
    }

    public static Point multiply(Point p1, Point p2){

        // Multiply point coordinates together
        Point p = new Point(p1.i * p2.i, p1.j * p2.j, p1.k * p2.k);
        
        return p;
        
    }

    public static Point divide(Point p1, Point p2){

        // Divide point coordinates together
        Point p = new Point(p1.i / p2.i, p1.j / p2.j, p1.k / p2.k);
        
        return p;
        
    }

    // Distance calculations
    private double distGeneral(Point p, int [] dirs){

        // Compute distance in each of the specified directions
        double val = 0.0;
        for (int ix: dirs){

            // Component distance squared ie: (p1.i - p2.i)^2
            val += Math.pow(this.getCoordByIndex(ix) - p.getCoordByIndex(ix), 2);
        }

        return Math.sqrt(val);
    }

    // Distance: 1D
    public double distI(Point p){
        return this.distGeneral(p, new int [] {0});
    }
    public double distJ(Point p){
        return this.distGeneral(p, new int [] {1});
    }
    public double distK(Point p){
        return this.distGeneral(p, new int [] {2});
    }

    // Distance: 2D
    public double distIJ(Point p){
        return this.distGeneral(p, new int [] {0, 1});
    }
    public double distIK(Point p){
        return this.distGeneral(p, new int [] {0, 2});
    }
    public double distJK(Point p){
        return this.distGeneral(p, new int [] {1, 2});
    }
    
    // Distance: 3D
    public double dist(Point p){
        return this.distGeneral(p, new int [] {0, 1, 2});
    }

    // Slope calculations
    private double slope_general(Point p, int ix, int jx){

        // Compute generic slope: dy /dx
        return (p.getCoordByIndex(jx) - this.getCoordByIndex(jx)) / (p.getCoordByIndex(ix) - this.getCoordByIndex(ix));
    }

    public double slope_ij(Point p){
        return this.slope_general(p, 0, 1);
    }

    public double slope_ik(Point p){
        return this.slope_general(p, 0, 2);
    }
    
    public double slope_ji(Point p){
        return this.slope_general(p, 1, 0);
    }
    
    public double slope_jk(Point p){
        return this.slope_general(p, 1, 2);
    }
    
    public double slope_ki(Point p){
        return this.slope_general(p, 2, 0);
    }
    
    public double slope_kj(Point p){
        return this.slope_general(p, 2, 1);
    }

    // Transform Matrices
    private DoubleMatrix getIRotationMatrix(double angle){
        /** Get the rotation matrix about i axis
        @param angle: rotation angle in radians
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */
        DoubleMatrix r = new DoubleMatrix(new double [][] 
            {
                {1.0, 0.0, 0.0, 0.0},
                {0.0, Math.cos(angle), -Math.sin(angle), 0.0},
                {0.0, Math.sin(angle), Math.cos(angle), 0.0},
                {0.0, 0.0, 0.0, 1.0},
            });
            
        return r;
    }

    private DoubleMatrix getJRotationMatrix(double angle){
        /** Get the rotation matrix about j axis
        @param angle: rotation angle in radians
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */
    
        DoubleMatrix r = new DoubleMatrix(new double [][] 
            {
                {Math.cos(angle), 0.0, Math.sin(angle), 0.0},
                {0.0, 1.0, 0.0, 0.0},
                {-Math.sin(angle), 0.0, Math.cos(angle), 0.0},
                {0.0, 0.0, 0.0, 1.0},
            });
            
        return r;
    }

    private DoubleMatrix getKRotationMatrix(double angle){
        /** Get the rotation matrix about k axis
        @param angle: rotation angle in radians
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */
        
        DoubleMatrix r = new DoubleMatrix(new double [][] 
            {
                {Math.cos(angle), -Math.sin(angle), 0.0, 0.0},
                {Math.sin(angle), Math.cos(angle), 0.0, 0.0},
                {0.0, 0.0, 1.0, 0.0},
                {0.0, 0.0, 0.0, 1.0},
            });
            
        return r;
    }

    private DoubleMatrix getTranslationMatrix(double i, double j, double k){
        /*Get the translation matrice

        i_new = i_old + offset_i
        j_new = j_old + offset_j
        k_new = k_old + offset_k

        @param i: offset in i direction
        @param j: offset in j direction
        @param k: offset in k direction
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */

        DoubleMatrix r = new DoubleMatrix(new double [][] 
        {
            {1.0, 0.0, 0.0, i},
            {0.0, 1.0, 0.0, j},
            {0.0, 0.0, 1.0, k},
            {0.0, 0.0, 0.0, 1.0},
        });
        return r;
    }

    private DoubleMatrix getScalingMatrix(double i, double j, double k){
        /*Get the scaling matrice assuming we are the center of the scale

        i_new = i_old * i_scale
        j_new = j_old * j_scale
        k_new = k_old * k_scale

        @param i: offset in i direction
        @param j: offset in j direction
        @param k: offset in k direction
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """
        */
        DoubleMatrix r = new DoubleMatrix(new double [][] 
        {
            {i, 0.0, 0.0, 0.0},
            {0.0, j, 0.0, 0.0},
            {0.0, 0.0, k, 0.0},
            {0.0, 0.0, 0.0, 1.0},
        });
        
        return r;

    }

    // Apply transforms
    public Point applyTransformations(DoubleMatrix [] t, Optional <Point> centerPoint){
        /*Apply a series of transformation matrices to the current coordinates

        @param t: transformation matrices to apply in specified order
        @param centerPoint: center point for the transformation
        @see: http://mikiobraun.github.io/jblas/javadoc/index.html
        */

        Point center;
        DoubleMatrix [] transforms;

        if (centerPoint.isPresent()){

            // Use the pointer
            center = centerPoint.get();

            // Get translations to center and back to normal
            DoubleMatrix t1 = this.getTranslationMatrix(-center.getI(), -center.getJ(), -center.getK());
            DoubleMatrix t3 = this.getTranslationMatrix(center.getI(), center.getJ(), center.getK());

            // Define array of transforms
            transforms = new DoubleMatrix [t.length + 2];

            // Apply center shifts first and last
            transforms[0] = t1;
            transforms[transforms.length - 1] = t3;

            // Copy transforms in the middle
            for (int ix = 0; ix < t.length; ix++){
                transforms[ix + 1] = t[ix];
            }

        }

        // Assume we're at the center
        else{

            // Grab a copy of the transformations
            transforms = Arrays.copyOf(t, t.length);
        }

        // Apply first transformation 
        DoubleMatrix xp = transforms[0].mmul(this.coords_hom);

        // Apply remaining transforms
        for (int ix = 1; ix < transforms.length; ix++){
            xp = transforms[ix].mmul(xp);
        }

        // Extract coordinates for point
        return new Point(xp.get(0, 0), xp.get(1, 0), xp.get(2, 0));
    }

    public Point applyTransformation(DoubleMatrix t, Optional <Point> centerPoint){
        /*Apply a transformation matrix to the current coordinates

        @param t: transformation matrices to apply in specified order
        @param centerPoint: center point for the transformation
        @see: http://mikiobraun.github.io/jblas/javadoc/index.html
        */
        return this.applyTransformations(new DoubleMatrix [] {t}, centerPoint);
    }

    // Transforms: Rotation
    public Point rotateI(double angle, Optional <Point> centerPoint){
        /*Rotate the point by an i axis in jk plane

        @param angle: amount of rotation in radians
        @param centerPoint: center of rotation
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */
    
        // Generate rotation and apply
        return this.applyTransformation(this.getIRotationMatrix(angle), centerPoint);
    }

    public Point rotateJ(double angle, Optional <Point> centerPoint){
        /*Rotate the point by an j axis in ik plane

        @param angle: amount of rotation in radians
        @param centerPoint: center of rotation
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */
    
        // Generate rotation and apply
        return this.applyTransformation(this.getJRotationMatrix(angle), centerPoint);
    }

    public Point rotateK(double angle, Optional <Point> centerPoint){
        /*Rotate the point by an k axis in ij plane

        @param angle: amount of rotation in radians
        @param centerPoint: center of rotation
        @see: https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */
    
        // Generate rotation and apply
        return this.applyTransformation(this.getKRotationMatrix(angle), centerPoint);
    }

    public Point rotateIJK(double [] angles, Optional <Point> centerPoint){
        /*Apply a series of rotations for ijk. Order of operations matter!

        @param angles: amount of rotation in each direction [radians]
        @param centerPoint: center of rotation
        @see: 
            https://en.wikipedia.org/wiki/Rotation_of_axes
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        */

        // Define rotation matrices
        DoubleMatrix rx = this.getIRotationMatrix(angles[0]);
        DoubleMatrix ry = this.getJRotationMatrix(angles[1]);
        DoubleMatrix rz = this.getKRotationMatrix(angles[2]);

        return this.applyTransformations(new DoubleMatrix [] {rx, ry, rz}, centerPoint);
    }

    // Transforms: Miscellaneous
    public Point reflectI(Optional <Point> centerPoint){
        /*Reflect the current point about i and the center point

        @param centerPoint: center of rotation
        */
        return this.scale(new double [] {-1.0, 1.0, 1.0}, centerPoint);
    }

    public Point reflectJ(Optional <Point> centerPoint){
        /*Reflect the current point about j and the center point

        @param centerPoint: center of rotation
        */
        return this.scale(new double [] {1.0, -1.0, 1.0}, centerPoint);
    }

    public Point reflectK(Optional <Point> centerPoint){
        /*Reflect the current point about k and the center point

        @param centerPoint: center of rotation
        */
        return this.scale(new double [] {1.0, 1.0, -1.0}, centerPoint);
    }

    public Point scale(double [] scale, Optional <Point> centerPoint){
        /*Scale the point about a central point

        i_new = (i_old - i_cen)* i_scale + i_cen
        j_new = (j_old - j_cen)* j_scale + j_cen
        k_new = (k_old - k_cen)* k_scale + k_cen

        Offset the coordinates by the center point.
        Scale the point, then add the offset back

        @param centerPoint: central point for scaling
        @param scale: scale factor(s)
        @see: https://math.stackexchange.com/questions/2093314/rotation-matrix-of-rotation-around-a-point-other-than-the-origin
        */

        // Generate scaling matrix and apply
        return this.applyTransformation(this.getScalingMatrix(scale[0], scale[1], scale[2]), centerPoint);
    }

    public Point scale(double scale, Optional <Point> centerPoint){
        /*Scale the point uniformly about a central point

        i_new = (i_old - i_cen)* i_scale + i_cen
        j_new = (j_old - j_cen)* j_scale + j_cen
        k_new = (k_old - k_cen)* k_scale + k_cen

        Offset the coordinates by the center point.
        Scale the point, then add the offset back

        @param centerPoint: central point for scaling
        @param scale: scale factor in each direction
        @see: https://math.stackexchange.com/questions/2093314/rotation-matrix-of-rotation-around-a-point-other-than-the-origin
        */

        // Generate scaling matrix and apply
        return this.applyTransformation(this.getScalingMatrix(scale, scale, scale), centerPoint);
    }

    public Point translate(double i, double j, double k){
        /*Offset the current point in the i,j or k directions

        i_new = i_old + offset_i
        j_new = j_old + offset_j
        k_new = k_old + offset_k

        @param i: offset in i direction
        @param j: offset in j direction
        @param k: offset in k direction
        @returns: translated point
        */
        
        return this.applyTransformation(this.getTranslationMatrix(i, j, k), Optional.empty());
    }

    public double getMag(){

        // Compute magnitude (sqrt(i^2 + j^2 + k^2))
        return Math.sqrt(Math.pow(this.i, 2) + Math.pow(this.j, 2) + Math.pow(this.k, 2));
    }

    public static void main(String[] args) {

        // System.out.println(value);
    }
}

// TODO: add subject observer pattern
